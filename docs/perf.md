Профилирование с помощью Perf
=============================

Perf - это userland tools for Linux Performance Counters.

Нам понадобятся две программы: perf и flamegraph.

Perf - это утилита ядра, так что требуемая версия зависит от используемого ядра.

В инструкции рассматривается вариант устанoвки в контейнере, с ручной
компиляцией perf.

Перед началом
-------------

Установка и работа с perf осуществляется от лица рута.
То есть с помощью с помощью скрипта `docker/client/bin/root`.

Компиляция в докере
-------------------

  ```
  mkdir /tmp/perf-linux-shallow

  cd /tmp/perf-linux-shallow

  git clone --depth 1 https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

  cd linux/tools/perf

  make

  cp perf /usr/bin
  ```

Профилирование программ
-----------------------

Директория сборки clippy: `/tmp/clippy-build`.

Теперь, когда у нас есть perf, запись исполнения программы `/.a.out`
осуществляется так:

```
perf record --call-graph dwarf ./a.out
```

В текущей директории появится файл `perf.data`.

Предлагается завести под записи отдельную директорию. Например
`/workspace/concurrency-course/perf-records/`.

```
mkdir /workspace/concurrency-course/perf-records/
```

См. также [примеры аргументов командной строки perf c объяснениями](https://www.brendangregg.com/perf.html)

Визуализация сырых данных
-------------------------

Для этого склонируйте репозиторий flamegraph. Оттуда нам нужны скрипты
`stackcollapse-perf.pl` и `flamegraph.pl`

```
git clone --depth 1 https://github.com/brendangregg/FlameGraph
```

Сгенерируйте картинку в директории с записями профилировщика

```
perf script
  | FlameGraph/stackcollapse-perf.pl
  | FlameGraph/flamegraph.pl
  > perf.svg
```

Откройте файл perf.svg в браузере, чтобы иметь возможность использовать
интерактивный зум.

Что такое cpu-flamegraph?
-------------------------

[Отвечает Брендан Грегг](https://www.brendangregg.com/FlameGraphs/cpuflamegraphs.html)
